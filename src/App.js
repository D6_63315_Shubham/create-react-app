// this toastr container will be used to show the toast messages
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

// import the required components from react-router-dom
// these are needed to configure client side routing
// Route: represents a route for a component
// Routes: collection of routes
// BrowserRouter: container for routes collection
// Link: used to jump to another component using its path
import { BrowserRouter, Routes, Route } from 'react-router-dom'

import Main from './pages/Main/main'

import ProfilePage from './pages/ProfilePage/ProfilePage'
import Itemsize from './pages/viewDetails/Itemsize'
import Posts from './pages/other/Posts'
import ToDo from './pages/other/ToDo'
import Gallery from './pages/other/Gallery'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Main />} />

        <Route path='/home' element={<ProfilePage />} />

        <Route path='/itemSize' element={<Itemsize />} />
        <Route path='/posts' element={<Posts />} />
        <Route path='/todo' element={<ToDo />} />
        <Route path='/gallery' element={<Gallery />} />
      </Routes>

      {/* this container is used to show toast messages */}
      <ToastContainer position='top-center' autoClose={1000} />
    </BrowserRouter>
  )
}

export default App
