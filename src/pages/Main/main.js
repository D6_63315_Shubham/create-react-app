import { motion, AnimatePresence } from 'framer-motion'
import React, { useState } from 'react'
import { useNavigate } from 'react-router'

import './Home.css'

export default function Main() {
  const navigate = useNavigate()

  return (
    <div style={{ overflowX: 'hidden' }}>
      <div>
        <div onLoad={chechLogin}>
          <div className='row shadow sticky-top'>
            <nav
              className='navbar navbar-expand-lg'
              style={{ backgroundColor: '#ff5a60' }}>
              <div className='container-fluid'>
                <button
                  className='navbar-toggler'
                  type='button'
                  data-bs-toggle='collapse'
                  data-bs-target='#navbarSupportedContent'
                  aria-controls='navbarSupportedContent'
                  aria-expanded='false'
                  aria-label='Toggle navigation'
                  style={{ backgroundColor: 'white' }}>
                  <span
                    className='navbar-toggler-icon'
                    style={{ backgroundColor: 'grey' }}></span>
                </button>
                <div
                  className='collapse navbar-collapse'
                  id='navbarSupportedContent'>
                  <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
                    <li className='nav-item'>
                      <a
                        className='nav-link active'
                        aria-current='page'
                        onClick={() => navigate('/home')}
                        id='headerBtn'>
                        Profile
                      </a>
                    </li>
                  </ul>
                  <div className=''>
                    <motion.button
                      className='btn btn-primary SignButton'
                      whileHover={{
                        backgroundColor: 'rgb(220, 222, 224)',
                        color: 'black',
                      }}
                      whileTap={{
                        backgroundColor: 'rgb(220, 222, 224)',
                        color: 'black',
                      }}
                      onClick={() => navigate('/posts')}>
                      Posts
                    </motion.button>
                  </div>
                  <div className=''>
                    <motion.button
                      className='btn btn-primary SignButton float-start'
                      whileHover={{
                        backgroundColor: 'rgb(220, 222, 224)',
                        color: 'black',
                      }}
                      whileTap={{
                        backgroundColor: 'rgb(220, 222, 224)',
                        color: 'black',
                      }}
                      onClick={() => navigate('/gallery')}>
                      Gallery
                    </motion.button>
                  </div>
                  <div className=''>
                    <motion.button
                      className='btn btn-primary SignButton float-start'
                      whileHover={{
                        backgroundColor: 'rgb(220, 222, 224)',
                        color: 'black',
                      }}
                      whileTap={{
                        backgroundColor: 'rgb(220, 222, 224)',
                        color: 'black',
                      }}
                      onClick={() => navigate('/todo')}>
                      ToDo
                    </motion.button>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
      <br />

      <br />
    </div>
  )
}
