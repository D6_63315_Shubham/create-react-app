import React from 'react'
import { useLocation } from 'react-router'

import { motion } from 'framer-motion'
import Header from '../Header/Header'

import config from '../../config'
import './Itemsize.css'

const ItemSize = () => {
  const { state } = useLocation()
  const { itemComp } = state
  console.log(itemComp)

  let string = config.serverURL + '/' + itemComp.profilepicture
  return (
    <div
      style={{ overflowX: 'hidden' }}
      className='fixedcontent'
      onLoad={() => {
        window.scrollTo(0, 0)
      }}>
      <Header />
      <br />
      <div style={{ backgroundColor: 'whitesmoke' }}>
        <br />
        <div
          className='container'
          style={{ backgroundColor: 'white', minHeight: '500px' }}>
          <div>
            <br />
            <br />
            <div>
              <div className='row'>
                <div className='col'>
                  <center>
                    <motion.img
                      src={string}
                      alt='profileImage'
                      className='shadow img-thumbnail'
                      style={{
                        height: '380px',
                        width: '480px',
                        borderRadius: '10px',
                        minWidth: '350px',
                        marginBottom: '2%',
                      }}
                      whileHover={{ scale: 1.03 }}
                    />
                  </center>
                </div>
                <div className='col'>
                  <div className='details_6'>
                    <p>{itemComp.name}</p>
                    <p>Username: {itemComp.username}</p>
                    <p>Email: {itemComp.email} yrs</p>
                    <p>Phone: {itemComp.phone}</p>
                    <p>Website: {itemComp.website}</p>
                  </div>
                </div>
              </div>
            </div>
            <br />
            <br />
          </div>

          <div className='container' style={{}}>
            <div className='row'>
              <div
                className='col'
                style={{
                  borderRightStyle: 'solid',
                  borderRightColor: 'lightgray',
                }}>
                <h2>Company</h2>

                <div className='row'>
                  <div className='col'>
                    <h4>Name</h4>
                  </div>
                  <div className='col'>
                    <h4>: {itemComp.company.name}</h4>
                  </div>
                </div>
                <br />

                <div className='row'>
                  <div className='col'>
                    <h4>catchphrase</h4>
                  </div>
                  <div className='col'>
                    <h3>: {itemComp.company.catchPhrase}</h3>
                  </div>
                </div>
                <br />

                <div className='row'>
                  <div className='col'>
                    <h4>bs</h4>
                  </div>
                  <div className='col'>
                    <h3>: {itemComp.company.bs}</h3>
                  </div>
                </div>
                <br />
              </div>
            </div>

            <hr />

            <div className='row'>
              <div
                className='col'
                style={{
                  borderRightStyle: 'solid',
                  borderRightColor: 'lightgray',
                }}>
                <h2>Address</h2>

                <div className='row'>
                  <div className='col'>
                    <h4>Street</h4>
                  </div>
                  <div className='col'>
                    <h4>: {itemComp.address.street}</h4>
                  </div>
                </div>
                <br />

                <div className='row'>
                  <div className='col'>
                    <h4>Suite</h4>
                  </div>
                  <div className='col'>
                    <h3>: {itemComp.address.suite}</h3>
                  </div>
                </div>
                <br />

                <div className='row'>
                  <div className='col'>
                    <h4>City</h4>
                  </div>
                  <div className='col'>
                    <h4>: {itemComp.address.city}</h4>
                  </div>
                </div>
                <br />

                <div className='row'>
                  <div className='col'>
                    <h4>zipcode</h4>
                  </div>
                  <div className='col'>
                    <h4>: {itemComp.address.zipcode}</h4>
                  </div>
                </div>

                <br />
              </div>
            </div>
          </div>
        </div>
        <br />
      </div>
    </div>
  )
}

export default ItemSize
