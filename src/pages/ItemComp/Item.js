import { useNavigate } from 'react-router'
import config from '../../config'
import './Item.css'

export default function Item(props) {
  const { itemComp } = props
  // let id = itemComp.id
  const navigate = useNavigate()

  let string = config.serverURL + '/' + itemComp.profilepicture
  return (
    <div className='col-md-4 col-sm-6'>
      <div className='itembox shadow'>
        <div class='card'>
          <img
            src={string}
            class='card-img-top'
            alt='profileImage'
            style={{ height: '300px' }}
          />
          <div className='card-body'>
            <h5 className='card-title'>
              {itemComp.name} {itemComp.username}
            </h5>

            <a
              className='btn btn-primary'
              onClick={() => {
                navigate('/itemSize', { state: { itemComp: itemComp } })
              }}>
              View Details
            </a>
          </div>
        </div>
      </div>
      <br />
    </div>
  )
}
