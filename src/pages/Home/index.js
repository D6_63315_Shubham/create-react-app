import { useNavigate } from 'react-router'

import { useEffect, useState } from 'react'
import axios from 'axios'
import { URL } from '../../config'
import { toast } from 'react-toastify'
import { Dropdown } from 'react-bootstrap'
import logo from '../../images/colorLogo.png'
import { Link } from 'react-router-dom'

const Admin = () => {
  const loginstatus = sessionStorage.getItem('currentloginStatus')
  const currentfirstName = sessionStorage.getItem('username')
  const navigate = useNavigate()
  const logout = () => {
    sessionStorage.clear()
    navigate('/')
  }
  const chechLogin = () => {
    if (loginstatus != 1) {
      var drop = document.getElementById('dropdown-basic')
      drop.disabled = true
    }
  }
  return (
    <div>
      return (
      <div onLoad={chechLogin}>
        <div className='row shadow sticky-top'>
          <nav
            className='navbar navbar-expand-lg'
            style={{ backgroundColor: '#ff5a60' }}>
            <div className='container-fluid'>
              <a className='navbar-brand'>
                <img
                  src={logo}
                  alt=''
                  id='headerlogoProfile'
                  onClick={() => navigate('/')}
                  style={{ cursor: 'pointer' }}
                />
              </a>
              <button
                className='navbar-toggler'
                type='button'
                data-bs-toggle='collapse'
                data-bs-target='#navbarSupportedContent'
                aria-controls='navbarSupportedContent'
                aria-expanded='false'
                aria-label='Toggle navigation'
                style={{ backgroundColor: 'white' }}>
                <span
                  className='navbar-toggler-icon'
                  style={{ backgroundColor: 'grey' }}></span>
              </button>
              <div
                className='collapse navbar-collapse'
                id='navbarSupportedContent'>
                <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
                  <li className='nav-item'>
                    <a
                      className='nav-link active'
                      aria-current='page'
                      onClick={() => navigate('/home')}
                      id='headerBtn'>
                      Home
                    </a>
                  </li>
                </ul>
                <div></div>
                <div>
                  <Dropdown>
                    <Dropdown.Toggle
                      variant='success'
                      id='dropdown-basic'
                      className='dropBtnd'>
                      Hi {currentfirstName} !
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => navigate('/myProfile')}>
                        My Profile
                      </Dropdown.Item>
                      {sessionStorage.getItem('role') === 'admin' ? (
                        <Dropdown.Item onClick={() => navigate('/myProfile')}>
                          Change Status
                        </Dropdown.Item>
                      ) : (
                        <Dropdown.Item></Dropdown.Item>
                      )}
                      <Dropdown.Item onClick={() => navigate('/search')}>
                        Search
                      </Dropdown.Item>
                      <Dropdown.Divider />
                      <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
      )
    </div>
  )
}
export default Admin
