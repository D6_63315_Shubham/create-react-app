import React, { useEffect, useState } from 'react'
import { motion } from 'framer-motion'
import './ProfilePage.css'
import axios from 'axios'
import Header from '../Header/Header'
import config from '../../config'

import { toast } from 'react-toastify'

import Item from '../ItemComp/Item'

export default function ProfilePage() {
  const scroolUp = () => {
    window.scrollTo(0, 0)
  }

  const [item, setitem] = useState([])
  // load all users
  const loadusers = () => {
    axios.get(config.serverURL + '/users/').then((response) => {
      const result = response.data
      console.log(result)
      if (result['status'] === 'success') {
        setitem(result['data'])
      } else {
        toast.error(result['error'])
      }
    })
  }

  // load the users as soon as the component gets loaded successfully
  useEffect(() => {
    loadusers()
  }, [])

  return (
    <motion.div
      style={{ overflowX: 'hidden' }}
      onLoad={scroolUp}
      className='fixedcontent'
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.3 }}>
      <Header />
      <br />
      <div style={{ backgroundColor: 'whitesmoke' }}>
        <br />
        <div
          className='container'
          style={{ backgroundColor: 'white', minHeight: '500px' }}>
          <br />

          <hr />
          <div className='row' style={{ padding: '2rem' }}>
            {item.map((peritem) => {
              return <Item itemComp={peritem} />
            })}
          </div>
        </div>
      </div>
    </motion.div>
  )
}
